
import java.net.ServerSocket;
import java.net.Socket;

/**************************
 * Assignment 1
 * Erwin Dave Salinas
 * 1000785308
 **************************/

/*****************************************
 * SOURCES:
 * 1.) Chapter 2 Notes from Networks class
 *****************************************/

/******************************************
 * Class: Server
 * Purpose: Listens for clients that 
 *			want to establish connections.
 ******************************************/
public class Server {

	public static void main(String args[]) throws Exception {
		
		/* Create welcoming socket at port 8080 */
		ServerSocket welcomeSocket = new ServerSocket(8080); 
		System.out.println("Listening");
		
	      while (true) {
	    	 /* Wait, on welcoming socket for contact by client */
	         Socket sock = welcomeSocket.accept();
	         System.out.println("Connected");
	          /* Handles request/response connections */
	         new Thread(new ResponseRequestHandler(sock)).start();
	      }
	}
}
