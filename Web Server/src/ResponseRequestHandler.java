import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Date;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;

/*********************
 * Assignment 1
 * Erwin Dave Salinas
 * 1000785308 
 *********************/

/*******************************************************************
 * SOURCES:
 * 1.) http://www.tutorialspoint.com/javaexamples/net_multisoc.htm 
 * 2.) http://cs.au.dk/~amoeller/WWW/javaweb/server.html 
 * 3.) Chapter 2 notes from Networks Class
 * *****************************************************************/

public class ResponseRequestHandler implements Runnable{

	
	Socket csocket; // Client socket name
	PrintStream poutToClient; // Used to forward response to client
	BufferedReader inFromClient; // Used to read request from client
	
	// Create client socket
	public ResponseRequestHandler(Socket sock){
		 this.csocket = sock; 
	}
	
	@Override
	public void run() {
		
		  // Will be use to retrieve the filename requested by browser
		   String filename="";
	    	  
			try {
				/* Create inputStream and outputStream which will be used to read request from client
				 * and send response to client
				 */
				inFromClient = new BufferedReader(new InputStreamReader(csocket.getInputStream()));
				poutToClient = new PrintStream(new BufferedOutputStream (csocket.getOutputStream()));
			 
				// Read HTTP request message 
				String request = inFromClient.readLine();
				
				// Tokenize HTTP request line (GET, POST, HEAD COMMANDS)
				/* request_token[0] should have "GET" 
				 * request_token[1] should have "directory/file.(html, jpg, etc..)
				 * request_token[2] should have "HTTP/1.1"
				 */
				String[] request_tokens = request.split(" ");
			
				// Checks if request was appropriately made by the browser(client) 
				if(request_tokens[0].equals("GET")){
					filename = request_tokens[1];
				}
	         
				/* Browsers look for index by default, so if ends with "/"
				   append  "index.html" */
				if (filename.endsWith("/")){
					filename = filename+"index.html";
				}
				
				while (filename.indexOf("/")==0){ 
					filename=filename.substring(1); // Removes first character of a string
					// In this case /filename become just filename
					// Code retrieve from:
					// http://stackoverflow.com/questions/4503656/in-java-removing-first-character-of-a-string
				} 
	           
				// Replace '/' with '\'
				// Retrieve from: http://www.java2s.com/Code/Java/File-Input-Output/Returnthepathwithinabasedirectory.htm
				filename=filename.replace('/', File.separator.charAt(0));

	           File file = new File(filename); // Create a file object with a filename
	           
	           if (file.isDirectory()) {
	        	   /* If file is a directory change \\ to / to work on browsers(client). */
	        	   System.out.println("checking");
	             filename=filename.replace('\\', '/'); 
	             /* Compose 301 response */
	             poutToClient.print("HTTP/1.0 301 Moved Permanently\r\n"+ // Status line (protocol status code
	            		 			"Location: /"+filename+"/\r\n\r\n"); // status phrase)
	             poutToClient.close();
	             return;
	           }

	           // Send File
	           InputStream sendFile=new FileInputStream(filename);

	           String fileType=fileTypes(filename);
	           
	           /* Compose 200 response */
	           poutToClient.print("HTTP/1.0 200 OK\r\n"+ // Status line (protocol status code status phrase)
	        		     "Connection: close\r\n" + 
	        		     "Date: " + new Date()+ "\r\n" + 
	        		     "Last-Modified:"+ new Date(file.lastModified())+ "\r\n"+
	        		     "Server: localhost\r\n" +
	        		     "Content-length"+file.length() + "\r\n"+/* header lines*/
			             "Content-type: "+fileType + "\r\n\r\n"); 
	           
	           
	           byte[] buffer=new byte[4096];
	           int length;
	           
	           /* Sending file contents to client */
	           try {
				while ((length= sendFile.read(buffer))>0) // Copy file content in bytes
				{
					poutToClient.write(buffer, 0, length);
				}
	           } catch (IOException e) {
					e.printStackTrace();
				}
	
				poutToClient.close();
	         }
		    /**********************************************
		     * Add a catch if file is not there.
		     * Inside catch, compose 404 Not Found message
		     * ********************************************/
	         catch (IOException x) {
	        	 poutToClient.println("HTTP/1.0 404 Not Found\r\n"+
	    	             "Content-type: text/html\r\n\r\n"+
	    	             "<html><head><h1>"+"HTTP 404 - File Not Found Error</h1></body></html>\n"+
	    	             "<html><head><h4>Date: " + new Date() +"</h4></body></html>\n");
	        	 poutToClient.close();
	         }
		
			try {
				csocket.close(); // Close client socket
			} 
			catch (IOException e) {
				e.printStackTrace();
			}	
	}
	
	/*********************************************************
	 * Method name: fileTypes
	 * Purpose: Checks to see the type of file for filename
	 * Input: filename in String
	 * Output: returns the file type of the filename
	 ***********************************************************/
	public String fileTypes(String filename)
	{
		/* By Default is text File */
		String fileType="text/plain";
		if (filename.endsWith(".html") || filename.endsWith(".htm")) // Type html
             fileType="text/html";
        else if (filename.endsWith(".jpg") || filename.endsWith(".jpeg")) // Type jpg
             fileType="image/jpeg";
        else if (filename.endsWith(".png")) // Type png
            fileType="image/png";
        else if (filename.endsWith(".css")) // Type CSS
             fileType="text/css";
        else if (filename.endsWith(".class")) //Types class
             fileType="application/octet-stream";
		
		return fileType;
	}
}