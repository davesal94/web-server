How to run program in Eclipse(Preferred):
1.) Unzip Web Server directory to your Eclipse workspace directory.
2.) Open Eclipse
3.) Go to file
      - Select import
     - Go to General -> Existing Project on WorkSpace - > On Select root Browse to where Web Server is located,
       select the Web Server directory
     - Click Next, then Finish.
4.) Go to Project Tab, click Clean, choose this project.
5.) RUN THE PROGRAM
      - Type localhost:8080 on a browser
      - index.html should pop up (Satisfying 200) it contains one image, and it has been cuztomized with a css file.
      - On index.html page, click Resume, you should see message (404 Not Found response) because Erwin_Salinas_Resume.html is not on server.
      - I have implemented the 301 Moved pernamently completely, however I am not sure how to test it. I tried moving the index.html file, but it gave the 404 
       Not Found response instead, but I am pretty sure I did the code correctly, I just don't know how to test it. Please let me know if you want to speak to me about
       this, I'll happily come to your office hour. Thanks!



How to run program in command prompt (Not Preferred).

1.) Unzip Web Server dir somewhere you want, go to src directory inside Web Server copy and paste the .java files to a new folder.
2.) Inside Web Server dir contains html, css, jpg files. Copy and paste those to the same new folder where .java files are.
3.) Open command prompt, browse to the new folder where everything was paste.
4.) Type javac Server.java
5.) Then type java Server
6.) - Type localhost:8080 on a browser 
     - index.html should pop up (Satisfying 200) it contains one image, and it has been cuztomized with a css file.
      - On index.html page, click Resume, you should see message (404 Not Found response) because Erwin_Salinas_Resume.html is not on server.
      - I have implemented the 301 Moved pernamently completely, however I am not sure how to test it. I tried moving the index.html file, but it gave the 404 
       Not Found response instead, but I am pretty sure I did the code correctly, I just don't know how to test it. Please let me know if you want to speak to me about
       this, I'll happily come to your office hour. Thanks!